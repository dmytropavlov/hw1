const express = require('express');
const fs = require('fs').promises;
const path = require('path');
const morgan = require('morgan');

const app = express();
app.use(express.json())
app.use(morgan('tiny'));

const port = 8080;

const dirpath = path.join(__dirname, 'docs');


app.post('/api/files', (req, res) => {
  (async () => {
    try {
      const keys = ['filename', 'content'];
      
      for (let key of keys) {
        if (JSON.stringify(req.body).search(key) < 0) {
          throw Error(`Please specify ${key} parameter`)
        }
      }
      const regex = /^.*\.(txt|log|json|yaml|xml|js)$/;

      if (!req.body.filename.match(regex)) {
        throw Error('FileFormat Exception');
      }

      const data = await fs.writeFile(dirpath + `/${req.body.filename}`, req.body.content);
      res.status(200);
      res.json({
        "message": "File created successfully"
      })
    }
    catch (err) {
      res.status(400);
      if (err.message === 'FileFormat Exception') {
        res.json({ "message": "Please specify correct FileFormat (log, txt, json, yaml, xml, js)" });
      }
      else {
        res.json({ "message": `${err.message}` });
      }
    }
  })();
})

app.get('/api/files', (req, res) => {
  (async () => {
    try {
      const data = await fs.readdir(dirpath);
      res.status(200);
      res.json({ "message": "Success", "files": data });
    }
    catch (err) {
      res.status(500);
      res.json({ "message": "Server error" })
    }
  })();
})

app.put('/api/files/:file', (req, res) => {
  (async () => {
    try {
      const keys = ['filename', 'content'];
      
      for (let key of keys) {
        if (JSON.stringify(req.body).search(key) < 0) {
          throw Error(`Please specify ${key} parameter`)
        }
      }

      const data = await fs.readFile(dirpath + `/${req.params.file}`);
      const secondData = await fs.writeFile(dirpath + `/${req.params.file}`, req.body.content, 'utf8');
      res.status(200);
      res.json({ "message": "File edited successfully" });
    }
    catch (err) {
      res.status(400);
      if(err.message.includes('Please')) {
        res.json({ "message": err.message})
      } else {
        res.json({ "message": `No file with ${req.params.file}` + " filename found" })
      }
    }
  })();
})

app.delete('/api/files/:file', (req, res) => {
  (async () => {
    try {
      await fs.unlink(dirpath + `/${req.params.file}`);
      res.status(200);
      res.json({
        "message": "File deleted successfully"
      });
    }
    catch (err) {
      res.status(400);
      res.json({
        "message": `No file with ${req.params.file}` + " filename found"
      })
    }
  })();
})

app.get('/api/files/:file', (req, res) => {
  (async () => {
    try {
      const data = await fs.readFile(dirpath + `/${req.params.file}`, 'utf8');
      const stats = await fs.stat(dirpath + `/${req.params.file}`);
      res.status(200);
      res.json({
        "message": "Success",
        "filename": req.params.file,
        "content": data,
        "extension": path.extname(req.params.file).slice(1, path.extname(req.params.file).length),
        "uploadedDate": stats.birthtime
      })
    }
    catch (err) {

      res.status(400);
      res.json({ "message": `No file with ${req.params.file}` + " filename found" })
    }
  })();
})

app.use((req, res) => {
  res.type('text/plain');
  res.status(404);
  res.send('404 - Not Found');
  console.log(req.query.pass);
})



app.listen(port, () => console.log(
  `Server started at http://localhost:${port}`));